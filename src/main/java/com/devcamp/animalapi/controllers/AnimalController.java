package com.devcamp.animalapi.controllers;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.animalapi.Cat;
import com.devcamp.animalapi.Dog;

@RestController
@RequestMapping("/")
@CrossOrigin
public class AnimalController {
    @GetMapping("/cats")
    public ArrayList<Cat> getCatsApi(){
        //task 4
        Cat cat1 = new Cat("Angel");
        Cat cat2 = new Cat("Moon");
        Cat cat3 = new Cat("Huhu");
        System.out.println(cat1);
        System.out.println(cat2);
        System.out.println(cat3);
        ArrayList<Cat> catList = new ArrayList<>();
        catList.add(cat1);
        catList.add(cat2);
        catList.add(cat3);
        return catList;
    }
    @GetMapping("/dogs")
    public ArrayList<Dog> getDogsApi(){
        //task 5
        Dog dog1 = new Dog("Becky");
        Dog dog2 = new Dog("Lu");
        Dog dog3 = new Dog("Hihi");
        System.out.println(dog1);
        System.out.println(dog2);
        System.out.println(dog3);
        ArrayList<Dog> dogList = new ArrayList<>();
        dogList.add(dog1);
        dogList.add(dog2);
        dogList.add(dog3);
        return dogList;
    }
}
